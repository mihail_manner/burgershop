import React from "react";
import Layout from "../components/layout";
import { graphql } from "gatsby";
import { itemShowcase } from "./item.module.css";

export default function ItemPage({ data }) {
  const post = data.markdownRemark;
  const imageSource = "../../" + post.frontmatter.picture;

  return (
    <Layout>
      <div className={`${itemShowcase} item-showcase`}>
        <h2>{post.frontmatter.title}</h2>
        <img src={imageSource} alt={post.frontmatter.picture_alt} />
        <div dangerouslySetInnerHTML={{ __html: post.html }} />
      </div>
    </Layout>
  );
}

export const query = graphql`
  query($slug: String!) {
    markdownRemark(fields: { slug: { eq: $slug } }) {
      html
      frontmatter {
        title
        picture
        picture_alt
        price
        type
        description
      }
      id
    }
  }
`;
