export const CartReducer = (state, action) => {
  switch (action.type) {
    case "ADD":
      state.cartItems.push(action.payload);
      localStorage.setItem("cart", JSON.stringify(state.cartItems));
      return {
        ...state,
        cartItems: [...state.cartItems],
      };
    case "REMOVE":
      const updatedCart = state.cartItems.filter(
        (node) => node.itemId !== action.payload
      );
      console.log(updatedCart);
      if (updatedCart.length > 0) {
        localStorage.setItem("cart", JSON.stringify(state.cartItems));
      } else {
        localStorage.removeItem("cart");
      }

      return {
        ...state,
        cartItems: [...updatedCart],
      };
    case "CLEAR":
      localStorage.removeItem("cart");
      return {
        ...state,
        cartItems: [],
      };
    case "LOAD":
      localStorage.setItem("cart", JSON.stringify(state.cartItems));
      return {
        ...state,
        cartItems: action.payload,
      };
    default:
      return state;
  }
};
