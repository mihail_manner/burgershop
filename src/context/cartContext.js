import React, { createContext, useReducer } from "react";
import { CartReducer } from "./cartReducer";
import { v4 as uuidv4 } from "uuid";

export const CartContext = createContext(undefined);

// Localstorage causes Server side rendering to fail, this is a janky hack to tackle that
const isBrowser = typeof window !== "undefined";

const CartContextProvider = ({ children }) => {
  let storage = [];
  if (isBrowser) {
    storage = localStorage.getItem("cart")
      ? JSON.parse(localStorage.getItem("cart"))
      : [];
  }

  const initialState = { cartItems: storage };
  const [state, dispatch] = useReducer(CartReducer, initialState);

  const addProduct = (product) => {
    product = {
      title: product.frontmatter.title,
      description: product.frontmatter.description,
      price: product.frontmatter.price,
      type: product.frontmatter.type,
      productId: product.id,
      itemId: uuidv4(),
    };
    dispatch({ type: "ADD", payload: product });
  };

  const removeItem = (itemId) => {
    dispatch({ type: "REMOVE", payload: itemId });
  };

  const clearAll = () => {
    console.log("miksei mitään tapahdu");
    dispatch({ type: "CLEAR" });
  };

  const contextValues = {
    addProduct,
    removeItem,
    clearAll,
    ...state,
  };

  return (
    <CartContext.Provider value={contextValues}>
      {children}
    </CartContext.Provider>
  );
};

export default CartContextProvider;
