---
title: "Cheese Burger"
price: 3.0
picture: purgeri.png
picture_alt: Cheese CartItem
type: Burgers
description: "Juicy cheeseburger"
---

Ainesosat: Pihvi, juusto, sämpylä, mausteet(mm. pippuri)

Herkullinen juustohamburilainen, jolla nälkä pysyy poissa pidemmän ajan!
