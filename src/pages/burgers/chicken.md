---
title: "Chicken Burger"
price: 4.0
picture: purgeri.png
picture_alt: Chicken CartItem
type: Burgers
description: "Tasty chickenburger"
---

Ainesosat: Kana, sämpylä, mausteet(mm. pippuri), majoneesi ja tomaatti

Maukas kanahamburilainen, jolla nälkä pysyy poissa pidemmän ajan!
