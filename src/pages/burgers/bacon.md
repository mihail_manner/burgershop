---
title: "Bacon Burger"
price: 3.8
picture: purgeri.png
picture_alt: Bacon CartItem
type: Burgers
description: "Juicy baconburger"
---

Ainesosat: Pekoni, pihvi, sämpylä, mausteet(mm. pippuri), majoneesi ja tomaatti

Mehevä pekonihamburilainen, jolla nälkä pysyy poissa pidemmän ajan!
