---
title: "Vegan Burger"
price: 3.2
picture: purgeri.png
picture_alt: Vegan Burger
type: Burgers
description: "Meatless burger"
---

Ainekset: Sämpylä, salaatti, tomaatti, kurkku, ilma ja majoneesi

Lihaton hamburilainen
