---
title: "Salad"
price: 1.8
picture: salad.png
picture_alt: Salad
type: Sides
description: "Fresh salad!"
---

Ainesosat: Salaatti, kurkku, tomaatti

Raikas salaatti kurkulla ja tomaatilla!
