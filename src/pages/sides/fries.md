---
title: "Fries"
price: 1.1
picture: fries.png
picture_alt: Fries
type: Sides
description: "Crispy fries!"
---

Ainesosat: Peruna, kumi, suola ja pippuri

Uppopaistettuja perunoita!

<iframe width="560" height="315" src="https://www.youtube.com/embed/SKeu2HBbpdo" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
