---
title: "Kokakola"
price: 1.2
picture: kola.png
picture_alt: Kokakola drink
type: Drinks
description: "Refreshing kola-drink"
---

Ainesosat: Kokis, kuplat ja vesi

Raikastava kola-juoma
