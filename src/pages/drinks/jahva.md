---
title: "Jahva"
price: 1.2
picture: jahva.png
picture_alt: Jahva drink
type: Drinks
description: "Refreshing jahva-drink"
---

Ainesosat: Appelsiini, kuplat ja vesi

Raikastava jahva-juoma
