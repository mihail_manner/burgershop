import React, { useState } from "react";
import Layout from "../components/layout";
import { itemContainer } from "./index.module.css";
import { graphql } from "gatsby";
import CartItem from "../components/cart/cartItem";

export default function Index({ data }) {
  const defaultCategory = data.site.siteMetadata.shopDefaultCategory;
  const [category, setCategory] = useState(defaultCategory);
  const displayItems = data.allMarkdownRemark.edges.filter(({ node }) => {
    return node.frontmatter.type === category;
  });
  return (
    <Layout>
      <h2>
        Tuotteet
        <select
          value={category}
          name="category"
          id="category_filter"
          onChange={(e) => setCategory(e.target.value)}
        >
          {data.allMarkdownRemark.edges
            .map(({ node }) => node.frontmatter.type)
            .filter((val, i, self) => {
              return self.indexOf(val) === i && val;
            })
            .map((type, i) => (
              <option key={i + "itemType"} value={type}>
                {type}
              </option>
            ))}
        </select>
      </h2>
      <div className={`${itemContainer} item-container`}>
        {displayItems.map(({ node }) => (
          <CartItem key={node.id} node={node} />
        ))}
      </div>
    </Layout>
  );
}

export const query = graphql`
  query getProducts {
    allMarkdownRemark(sort: { fields: [frontmatter___price] }) {
      edges {
        node {
          fields {
            slug
          }
          frontmatter {
            title
            picture
            picture_alt
            price
            type
            description
          }
          id
          excerpt
        }
      }
      totalCount
    }
    site {
      siteMetadata {
        shopDefaultCategory
      }
    }
  }
`;
