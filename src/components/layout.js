import React from "react";
import { layout } from "./layout.module.css";
import CartContextProvider from "../context/cartContext";
import Cart from "./cart/cart";
import { Link } from "gatsby";

export default function Layout({ children }) {
  return (
    <div className={`${layout} layout`}>
        <Link to="/">
            <h1>Mihail's Burgerplace</h1>
        </Link>

      <CartContextProvider>
        <Cart />
        {children}
      </CartContextProvider>
    </div>
  );
}
