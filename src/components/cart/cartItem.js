import React, { useContext } from "react";
import { Link } from "gatsby";
import { item, cartButton } from "../../pages/index.module.css";
import { CartContext } from "../../context/cartContext";

export default function CartItem({ node }) {
  const { addProduct } = useContext(CartContext);
  return (
    <div key={node.id} className={`${item} item`} >
      <Link to={node.fields.slug}>
        <h3>
          {node.frontmatter.title}
          <br />
          {node.frontmatter.price}€
        </h3>
        <img
          src={"/" + node.frontmatter.picture}
          alt={node.frontmatter.picture_alt}
        />
      </Link>
      <button className={`${cartButton} cart-button`} onClick={() => addProduct(node)}>
        +
      </button>
      <p>{node.frontmatter.description}</p>
    </div>
  );
}
