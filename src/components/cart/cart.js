import React, { useEffect, useState } from "react";
import { useContext } from "react";
import { CartContext } from "../../context/cartContext";
import {
  cartIcon,
  cartModal,
  container,
  tableWrapper,
} from "./shoppingCart.module.css";

export default function Cart() {
  const { cartItems, clearAll, removeItem } = useContext(CartContext);
  const [modalOpen, setModalState] = useState(false);
  const cartTotalSum = cartItems
    .reduce((total, item) => total + item.price, 0)
    .toFixed(2);

  useEffect(() => {});

  const closeModal = (e) => {
    if (e.target.classList.contains(cartModal)) {
      setModalState(false);
    }
  };

  const paymentButtonOnClick = () => {
    alert(
      "Rahat viety tililtäsi ja ruuat tulee joskus jos tulee :) Hyvää päivän jatkoa!"
    );
    clearAll();
  };

  const removeItemOnClick = (item) => {
    removeItem(item.itemId);
  };

  const CartIcon = () => {
    return (
      <div className={cartIcon} id="cart-icon">
        <button onClick={() => setModalState(true)}>
          <img src={"/cart.svg"} alt="Shopping cart"/>
        </button>
        <span>{cartItems.length}</span>
      </div>
    );
  };

  const CartModal = () => {
    const Header = () => (
        <>
          <h3>
            Shopping Cart
            <button
                id="close-cart"
                onClick={() => setModalState(false)}
                aria-label="Close cart"
            />
          </h3>
          <hr/>
        </>
    )

    const CartTable = () => (
        <div className={`${tableWrapper} table-wrapper`}>
          <table id="cart-table">
            <thead>
            <tr>
              <th>Type</th>
              <th>Description</th>
              <th>Price</th>
              <th>&nbsp;</th>
            </tr>
            </thead>
            <tbody>
            {cartItems.map((item) => (
                <tr key={item.itemId}>
                  <td>{item.title}</td>
                  <td>{item.description}</td>
                  <td>{item.price} €</td>
                  <td>
                    <button
                        onClick={() => removeItemOnClick(item)}
                        onKeyDown={() => removeItemOnClick(item)}
                    >
                      <img src={"/trash.svg"} alt="remove icon" />
                    </button>
                  </td>
                </tr>
            ))}
            </tbody>
            <tfoot>
            <tr>
              <th>Total</th>
              <th>&nbsp;</th>
              <th>{cartTotalSum} €</th>
              <th>&nbsp;</th>
            </tr>
            </tfoot>
          </table>
          <button onClick={paymentButtonOnClick}>Pay now!</button>
        </div>
    )

    return (
      <div
        className={cartModal}
        id="cart-modal"
        onClick={closeModal}
        role="button"
        tabIndex={0}>
        <div className={`${container} container`}>
          <Header/>
          {cartItems.length > 0 ? <CartTable/> : (<p>Your shopping cart is empty</p>)}
        </div>
      </div>
    );
  };

  return modalOpen ? <CartModal/> : <CartIcon/>;
}
