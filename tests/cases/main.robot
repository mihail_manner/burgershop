*** Settings ***

Library     SeleniumLibrary
Library     String
Suite Teardown  Close All Browsers
Suite Setup  Before Starting

*** Variables ***
${URL}      http://localhost:8000/
${BROWSER}      Firefox
${EXPECTED_TEXT}    Mihail's Burgerplace
${CART_EMPTY_TEXT}      Your shopping cart is empty
${ITEMS}       css:div.item
${CART-ICON}        css:#cart-icon
${CART-MODAL}       css:#cart-modal
${CART-MODAL-CLOSE}     css:#close-cart
${CART-TABLE}       css:#cart-table
${CART-PAYMENT-BUTTON}      css:.table-wrapper > button

*** Test Cases ***

Index Page Renders
    Verify Index Page

Add To Cart Is Visible On Mouse Over
    Mouse Over      ${ITEMS}
    Wait Until Element Is Visible       ${ITEMS} > button

Cart Icon Updates After Item Is Added
    Add First Item To Cart
    Element Text Should Be      css:#cart-icon > span        1

Cart Contains Added Item
    Add First Item To Cart
    Mouse Over       ${CART-ICON}
    ${itemDescription}=     Get Text        ${ITEMS} > p
    Open Cart Modal
    Element Text Should Be       ${CART-TABLE} > tbody > tr > td:nth-child(2)     ${itemDescription}
    Close Cart Modal

Cart Item Removed
    Open Cart Modal
    Remove Item From Cart
    Cart Should Be Empty
    Close Cart Modal

Cart Payment Completes
    Add First Item To Cart
    Open Cart Modal
    Click Element       ${CART-PAYMENT-BUTTON}
    Handle Alert
    Cart Should Be Empty
    Close Cart Modal

Category Change Updates Items
    Change Item Category        1
    ${val}=       Get Current Item Category
    Page Should Contain Items From Category       ${val}
    Reset Category

Item Page
    ${category}=        Get Current Item Category
    ${categoryInLower}=         Convert To Lower Case        ${category}
    Click Link      ${ITEMS} > a
    Location Should Contain         ${categoryInLower}

Item Page Contains Link Back To Main Page
    Page Should Contain Link        /



*** Keywords ***
Open Browser To Main Page
    Open Browser    ${URL}  ${BROWSER}
    Maximize Browser Window
    Wait Until Page Contains    ${EXPECTED_TEXT}

Verify Index Page
     Wait Until Page Contains Element    tag:select  # Category select box

Element Text Should Be
    [Arguments]     ${target}       ${value}
    ${temp}=        Get Element Text       ${target}
    Should Be Equal As Strings       ${temp}     ${value}

Page Should Contain Items From Category
    [Arguments]     ${category}
    Page Should Contain Link      xpath: //a[contains(@href, ${category})]

Cart Should Be Empty
    Element Text Should Be      ${CART-MODAL} > .container > p      ${CART_EMPTY_TEXT}

Change Item Category
    [Arguments]     ${index}
    Select From List By Index    xpath=//select     ${index}

Remove Item From Cart
    Click Element       ${CART-TABLE} > tbody > tr > td:last-child > button

Get Element Text
    [Arguments]     ${selector}
    [Return]        ${val}
    Wait Until Page Contains Element        ${selector}
    ${val}=     Get Text        ${selector}

Get Current Item Category
    [Return]        ${category}
    ${category}=        Get Selected List Label     id:category_filter

Open Cart Modal
   Click Element       ${CART-ICON} > button
   Wait Until Page Contains Element        ${CART-MODAL}

Close Cart Modal
    Click Element       ${CART-MODAL-CLOSE}
    Wait Until Page Does Not Contain Element     ${CART-MODAL}

Add First Item To Cart
    Mouse Over      ${ITEMS}
    Wait Until Element Is Visible       ${ITEMS} > button
    Click Element       ${ITEMS} > button
    Mouse Out       ${ITEMS}

Reset Category
    Change Item Category        0

Before Starting
    Open Browser To Main Page
    #Set Selenium Speed      0.1
    Execute Javascript      window.localStorage.removeItem('cart');
