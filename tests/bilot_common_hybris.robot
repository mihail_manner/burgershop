*** Settings ***

Documentation  This is a common file with settings and our test keywords.
Resource       bilot_common.robot
Library	SeleniumLibrary	timeout=${DEFAULT_TIMEOUT}	implicit_wait=${DELAY}	run_on_failure=Run on Failure

#Library	Remote	http://localhost:8270
#Resource	../lib/core_robot_hybris/keywords/B2B Keywords/B2B-AdminCockpit-HighLevelKeywords.txt
#Resource	../lib/core_robot_hybris/keywords/hMC Keywords/hMC-HighLevelKeywords.txt
#Resource          ../lib/core_robot_hybris/keywords/Common Keywords/Common-HighLevelKeywords.txt
#Variables         ../lib/core_robot_hybris/variables/commonLocalVariables.py
#Variables         ../lib/core_robot_hybris/variables/commonGlobalVariables.py
#Resource          ../lib/core_robot_hybris/keywords/CMSCockpit Keywords/CMSC-HighLevelKeywords.txt
#Resource          ../lib/core_robot_hybris/keywords/ProductCockpit Keywords/PROC-HighLevelKeywords.txt
#Resource          ../lib/core_robot_hybris/keywords/hMC Keywords/hMC-HighLevelKeywords.txt


*** Variables ***

# Set here variables that are relevant for all tests. Set data for products and users in the data files.
${SERVER}        http://bilotsmartch1:9001
${SHOP}        ${SERVER}/bilotstorefront/
${CAMPAIGN_LINK}        ${SHOP}/buy
${ERP_OFFLINE_URL}        ${SERVER}/bilotImport/import/offline/DEV/state/true
${ERP_ONLINE_URL}        ${SERVER}/bilotImport/import/offline/DEV/state/false
${HMC_URL}        ${SERVER}/hmc/hybris
${BACKOFFICE}        ${SERVER}/backoffice


${B2BDefaultUrl}	${SHOP}	# Sets Hybris variable
${INVALID_USER}    invalid
${INVALID_PASSWD}  invalid
${TEMPLATE_NAME}	test_template_b2b
# The environment to use. DEV (Development) id default, or QA.
${ENV}		DEV
${DATA_PATH}	../data

*** Keywords ***

Open Browser To Shop
    Open Browser  ${SHOP}/login	${BROWSER}
    Maximize Browser Window
    Set Selenium Speed	${DELAY}
    Run Keyword If	'${BROWSER}' == 'IE'	Sleep	5
    # Test only page structure. To make the tests as generic as possible
    # we only verify that certain elements are in the page.
    Verify Main Page

Open Browser with Campaign To Shop
	[Documentation]	Opens the shop with campaign product
	[Arguments]	${link}
    Open Browser  ${link}	${BROWSER}
    Maximize Browser Window
    Set Selenium Speed	${DELAY}
    Run Keyword If	'${BROWSER}' == 'IE'	Sleep	5

Open Browser To Backoffice and login as user admin
    Open Browser  ${BACKOFFICE}	${BROWSER}
    Maximize Browser Window
    Set Selenium Speed	${DELAY}
    Run Keyword If	'${BROWSER}' == 'IE'	Sleep	5
	Wait Until Page Contains Element	j_username
	Input Text	j_username	${USERADMIN_USERNAME}
	Input Text	j_password	${USERADMIN_PASSWORD}
	Sleep   32
    Bilot Click Link	css=#lgBtn
    Wait Until Page Contains Element	//*/div[@title='Logout']	# Verifying that the login went fine

Clean Up Test with Logoff
#    Empty cart
	Run Keyword And Ignore Error	Capture Page Screenshot
	${previous kw}=	Register Keyword To Run On Failure	Nothing	# Disables run-on-failure functionality and stores the previous kw name in a variable.
	#Run Keyword And Ignore Error	Log user off
	Delete All Cookies
	Run Keyword And Ignore Error	Close Browser
	Register Keyword To Run On Failure	${previous kw}	# Restore to the previous keyword.

Clean Up Test without Logoff
	Run Keyword And Ignore Error	Capture Page Screenshot
	${previous kw}=	Register Keyword To Run On Failure	Nothing	# Disables run-on-failure functionality and stores the previous kw name in a variable.
	Delete All Cookies
	Run Keyword And Ignore Error	Close Browser
	Register Keyword To Run On Failure	${previous kw}	# Restore to the previous keyword.

Clean Up Test with Online Mode and Logoff
    Clean Up Test with Logoff
    Online Mode

Verify Main Page
	[Documentation]	Verifies if the expected elements on the main page are present
	Wait Until Page Contains Element	css=.siteLogo	# The search box

Login to Shop
	[Documentation]	Logs in the user with given username and pasword to the shop. After login cart is emptied.
	[Arguments]	${user_name}	${user_password}
#	Bilot Click Link	Login	#FIXME	the current Login link has no ids, we need to refer via the name
#	Bilot Click Link	//*/div[@class='myaccount_link']
	Wait Until Page Contains Element	j_username
	Input Text	j_username	${user_name}
	Input Text	j_password	${user_password}
	Sleep   2
	Select Checkbox    css=#LoginTerms
    Bilot Click Link	css=#loginButton
    Wait Until Page Contains Element	//*/div[@class='logged_in']	# Verifying that the login went fine
    Empty cart
    Bilot Click Link	//*/div[@class='headerLogo']/div/div/a

Search for Products
	[Documentation]	Searches for products based on the given phrase
	[Arguments]	${phrase}
	Enter search word   ${phrase}
	Bilot Click Link	css=.siteSearchSubmit

Enter search word
    [Documentation]	Enter the given phrase
    [Arguments]	${phrase}
    Wait Until Page Contains Element	bilotsearch
    Input Text	bilotsearch	${phrase}

Add Product with ID
	[Documentation]	Adds a product with given ID to the basket
    [Arguments]	${productID}
	Search for Products	${productID}
    Bilot Click Link  //*/form[@id='addToCartForm${productID}']/div/div[@class='cartButton add1ToCartButtontrue']
    Wait Until Element Is Visible	css=#addToCartLayer

Expect Product to be Out-Of-Stock
	[Documentation]	Expects that the product with given id is on the list of search results, but is out of stock.
	[Arguments]	${productID}
	Search for Products	${productID}
    Wait Until Page Contains Element	//*/input[@value='${productID}']
    Bilot Element Text Should Be    .stockData      0

Go to Basket
	[Documentation]	Goes to the basket
	Bilot Click Link	css=.miniCart a
	Wait Until Page Contains Element	css=#checkoutOrderDetails

Go to Basket Selecting Template Option
	[Documentation]	Goes to the basket by choosing Template mode
	Bilot Mouse Over	css=#searchCart
	Bilot Click Link	toTemplateButton
	Wait Until Page Contains Element	css=#checkoutOrderDetails

Go to Basket Selecting Basket Option
	[Documentation]	Goes to the basket by choosing Template mode
	Bilot Mouse Over    xpath=//*/div[@id='searchCart']/div[@class='miniCart']/a[@class='bilotButton']
	Bilot Click Link	toBasketButton
	Wait Until Page Contains Element	css=#checkoutOrderDetails

Check Template Grouping
	[Documentation]     Check that grouping exists for order template (subtitles)
	Wait Until Page Contains Element    xpath=//*/div[@id='group_0'][text() = '${SUBTITLE1}']    # Milk/Milk substituts
	Wait Until Page Contains Element    xpath=//*/div[@id='group_1'][text() = '${SUBTITLE2}']    # Bread


Create Order
#    Bilot Click Link    css=#Terms1
    Bilot Click Link    css=.placeOrderButton
    Wait Until Page Contains Element	//*/div[@class='orderHead']

Create Template
    Bilot Click Link	css=#ordertemplateButton
    Wait Until Page Contains Element	css=#accountNav_templates

Log Off
	Bilot Click Link	//*/div[@class='myaccount_link']
    Bilot Click Link    css=#header_signOut

Log Off Backoffice
    Bilot Click Link	xpath=(//div[@title='Logout']//*/img)


Change Header Delivery to Last Possible Date
	[Documentation]	Selects the last possible data when the delivery can happen
	${original_header_date}=	Bilot Get Element Value	requestedDate2
	Bilot Click Link	css=#requestedDate2
	Bilot Click Link    css=.ui-datepicker-next
	Bilot Click Link	xpath=(//*/td[not(contains(@class,'ui-datepicker-unselectable'))]/a)[last()]
	${new_header_date}=	Bilot Get Element Value	requestedDate2
	Should Not Be Equal	${original_header_date}	${new_header_date}
	Bilot Click Link    ajaxCartItems       #close calendar
	# Caheck that all item levels have been changed
	${number_of_items}=	Get Matching Xpath Count	//*/table[@id='your_cart']/tbody/tr
	:FOR	${index}	IN RANGE	${number_of_items}
 	\	Log	${index}
	\	${new_item_date}=	Bilot Get Element Text	requestedDate${index}
	\	Should Be Equal	${new_header_date}	${new_item_date}

Change Item Delivery Date to First Possible Date
	[Documentation]	Changes the given item (indexed from 10, which is the bottom product) to the first possible delivery date
	[Arguments]     ${item_number}      ${item_index}
	${original_item_date}=	Bilot Get Element Value     requestedDate_${item_number}_${item_index}
	Bilot Click Link	css=#requestedDate_${item_number}_${item_index}
	Bilot Click Link    css=.ui-datepicker-next
	Bilot Click Link	xpath=((//*/table[@class='ui-datepicker-calendar'])/tbody/tr/td[not(contains(@class,'ui-datepicker-unselectable'))])[1]
    Sleep   3   #wait for the ajax to kick in
	${new_item_date}=	Bilot Get Element Value  	requestedDate_${item_number}_${item_index}
	Should Not Be Equal	${original_item_date}	${new_item_date}
	Bilot Click Link    ajaxCartItems       #close calendar

Is MyAccount Page Visible
	[Documentation]  Check whether My Account -page is already opened
	[Return]    ${isOpen}
	${status}	${value} =  Run Keyword And Ignore Error    Page Should Contain Element     xpath=//*/div[@class='cust_acc']/div[@class='cust_acc_tile']
    ${isOpen} =     Run Keyword And Ignore Error    Run Keyword If  '${value}' == 'PASS'     Set Variable    ${True}

Open MyAccount Page
    [Documentation]  Open My account -page in SmartChannel
	Bilot Click Link	//*/div[@class='myaccount_link']
	Wait Until Page Contains Element	//*/a[@id='myaccount_link']
    Bilot Click Link    //*/a[@id='myaccount_link']
    Wait Until Element Is Visible   css=#updateProfile

Open OrderHistory Page
    [Documentation]  Open Order History page
    ${isOpen}=  Is MyAccount Page Visible
    Run Keyword And Ignore Error    Run Keyword If		${isOpen} != ${True}	Open MyAccount Page
    Bilot Click Link    css=#order_history_link

Open Order From ResultList
    [Documentation]  Open given order from resultlist
    [Arguments]  ${orderNumber}
    Search Order By Order Number    ${orderNumber}
    Bilot Click Link    css=#orderHistory_orderNumber_link_${orderNumber}

Search Order By Order Number
    [Documentation]     Search for given sales order
    [Arguments]     ${orderNumber}
    Wait Until Element Is Visible   orderId
    Input Text	orderId     ${orderNumber}
    Bilot Click Link    filter

Search Orders By PO Number
    [Documentation]     Search for sales orders by PO number
    [Arguments]     ${poNumber}
    Wait Until Element Is Visible   purchaseOrderNumber
    Input Text	purchaseOrderNumber     ${poNumber}
    Bilot Click Link    filter

Search Orders By Product
    [Documentation]     Search for sales orders by Product
    [Arguments]     ${productQuery}
    Wait Until Element Is Visible   purchaseOrderNumber
    Input Text	productId     ${productQuery}
    Bilot Click Link    //*/div[@id='productId-div']/ul/li[@class='ui-menu-item'][1]/a
    Bilot Click Link    filter

Search Orders By Creation Date
    [Documentation]     Search for sales orders by Creation Date
    [Arguments]     ${monthYear}    ${day}
    Select Datepicker Date  dateFrom  ${monthYear}   //*/div[@id='ui-datepicker-div']/table[@class='ui-datepicker-calendar']/tbody/tr/td/a[text() = '${day}']
    Bilot Click Link    filter

Search Orders By Requested Delivery Date
    [Documentation]     Search for sales orders by Requested Delivery Date
    [Arguments]     ${monthYear}    ${day}
    Select Datepicker Date  css=#reqDateFrom    ${monthYear}   //*/div[@id='ui-datepicker-div']/table[@class='ui-datepicker-calendar']/tbody/tr/td/a[text() = '${day}']
    Bilot Click Link    filter

Search Orders By Creation Date Range
    [Documentation]     Search for sales orders by Creation Date range
    [Arguments]     ${monthYearFrom}    ${dayFrom}     ${monthYearTo}  ${dayTo}
    # browse datepicker for September, 2014
    Select Datepicker Date  dateFrom  ${monthYearFrom}   //*/div[@id='ui-datepicker-div']/table[@class='ui-datepicker-calendar']/tbody/tr/td/a[text() = '${dayFrom}']
    Select Datepicker Date  dateTo  ${monthYearTo}   //*/div[@id='ui-datepicker-div']/table[@class='ui-datepicker-calendar']/tbody/tr/td/a[text() = '${dayTo}']
    Bilot Click Link    filter

Search Orders By Requested Delivery Date Range
    [Documentation]     Search for sales orders by Creation Date range
    [Arguments]     ${monthYearFrom}    ${dayFrom}     ${monthYearTo}  ${dayTo}
    # browse datepicker for September, 2014
    Select Datepicker Date  reqDateFrom  ${monthYearFrom}   //*/div[@id='ui-datepicker-div']/table[@class='ui-datepicker-calendar']/tbody/tr/td/a[text() = '${dayFrom}']
    Select Datepicker Date  reqDateTo  ${monthYearTo}   //*/div[@id='ui-datepicker-div']/table[@class='ui-datepicker-calendar']/tbody/tr/td/a[text() = '${dayTo}']
    Bilot Click Link    filter

Search Orders By Creation Date and PO Number
    [Documentation]     Search sales orders by Creation Date and PO Number
    [Arguments]     ${date}     ${day}     ${poNumber}
    Wait Until Element Is Visible   purchaseOrderNumber
    Input Text	purchaseOrderNumber     ${poNumber}
    Select Datepicker Date  dateFrom  ${date}   //*/div[@id='ui-datepicker-div']/table[@class='ui-datepicker-calendar']/tbody/tr/td/a[text() = '${day}']
    Bilot Click Link    filter

Select Datepicker Date
    [Documentation]     Select given day from datepicker
    [Arguments]     ${dateElem}     ${expectedMonthYear}    ${clickElement}
    Input Text  ${dateElem}    ${Empty}    # open the datepicker
    ${monthyear}=   Get Datepicker MonthYear
    :FOR    ${Index}    IN RANGE    1   30
    \   Run Keyword If  '${monthyear}' == '${expectedMonthYear}'   Exit For Loop
    \   Bilot Click Link    //*/div[@id='ui-datepicker-div']//*/a[contains(@class, 'ui-datepicker-prev')]
    \   ${monthyear}=   Get Datepicker MonthYear
    Bilot Click Link    ${clickElement}

Get Datepicker MonthYear
    [Documentation]     Return current month + year from datepicker
    [Return]    ${monthyear}
    ${month}=	Bilot Get Element Text  //*/div[@id='ui-datepicker-div']//*/div[@class='ui-datepicker-title']/span[@class='ui-datepicker-month']
    ${year}=	Bilot Get Element Text  //*/div[@id='ui-datepicker-div']//*/div[@class='ui-datepicker-title']/span[@class='ui-datepicker-year']
    ${monthyear}=   Catenate    ${month}  ${year}

Check Download OrderConfirmationButton Is Visible
    [Documentation]  Check whether order confirmation download button is visible
    Wait Until Element Is Visible   xpath=//*/button[contains(@id, 'orderConfDownloadButton_')]

Check Download Invoice Button Is Visible
    [Documentation]  Check whether invoice download button is visible
    Wait Until Element Is Visible   xpath=//*/button[contains(@id, 'invoicePdfDownloadButton_')]

Product details with ID
	[Documentation]	Searches and navigates to product details page
	[Arguments]	${productID}
	Search for Products	${productID}
	Bilot Click Link	css=#productCode_${productID}
	Wait Until Element Is Visible	//*/h1[contains(text(), '${productID}')]

Offline Mode
	[Documentation]	Calls offline mode HTTP endpoint to toggle offline
	Open Browser	${ERP_OFFLINE_URL}	${BROWSER}
	Set Selenium Speed	${DELAY}
	Page Should Contain	true
	Close Browser

Online Mode
	[Documentation]	Calls offline mode HTTP endpoint to toggle offline
	Open Browser	${ERP_ONLINE_URL}	${BROWSER}
	Set Selenium Speed	${DELAY}
	Page Should Contain	true
	Close Browser

Check Order Search Fields Are Visible
    [Documentation]     Checks that order search fields are present
    Wait Until Page Contains Element    orderId
    Wait Until Page Contains Element    purchaseOrderNumber
    Wait Until Page Contains Element    productId
    Wait Until Page Contains Element    dateFrom
    Wait Until Page Contains Element    dateTo

Reset Order Search Fields
    [Documentation]     Reset search fields in Order search
    Input Text    orderId   ${Empty}
    Input Text    purchaseOrderNumber   ${Empty}
    Input Text    productId     ${Empty}
    Input Text    dateFrom  ${Empty}
    Input Text    dateTo    ${Empty}
    Input Text    reqDateFrom   ${Empty}
    Input Text    reqDateTo   ${Empty}

Find Order From ResultList
    [Documentation]  Check that given order is present resultlist
    [Arguments]  ${orderNumber}
    Wait Until Page Contains Element    css=#orderHistory_orderNumber_link_${orderNumber}

Check Order Resultlist Contains At Least One Result
    [Documentation]     Check that order search returned at least one order
    Wait Until Element Is Visible   xpath=//*/table[contains(@class, 'orderListTable')]/tbody/tr[1]/td[1]

Check Order Resultlist Contains At Least One Result For Date
    [Documentation]     Check that order search returned at least one order
    [Arguments]     ${date}
    Wait Until Page Contains Element    xpath=//*[@headers='header4']/*[contains(text(),'${date}')]

Delete order template
    [Documentation]     Deletes given order template
    [Arguments]     ${templateId}
    Open order template     ${templateId}
    Click Button    xpath=//button[@id="deleteTemplateButton"]

Open order template
    [Documentation]     Deletes given order template
    [Arguments]     ${templateId}
    Open MyAccount Page
    Bilot Click Link    css=#accountNav_templates
    Bilot Click Link    css=#myAccountOrderDetailsUrl_${templateId}


Check Print Button Exists
    [Documentation]     Check that print button exists
    Wait Until Page Contains Element    printButton

Change Language
    [Documentation]     Change language from language selector
    [Arguments]     ${language}
    Select From List By Value   lang-selector   ${language}

    
Enter customer
    [Documentation]	Enter the given phrase
    [Arguments]	${phrase}
    Wait Until Page Contains Element	bilotCustomerSearch
    Input Text	bilotCustomerSearch	${phrase}
    Bilot Click Link  //*/a[@href='/heinob2bstorefront/heinob2bstorefront/fi/EUR/bilotCatalog/selectUnit?unit=${phrase}']	
    Wait Until Page Contains Element	//*/span[contains(string(),'${phrase}')]


Empty cart
    [Documentation]	Delete basket items
    # move mouse away from the catalog overlay
    Bilot Mouse Over    css=.headerLogo
    Sleep   1
    Bilot Click Link    xpath=//*/div[@id='searchCart']/div[(contains(@class,'miniCart'))]/a[@class='bilotButton']
    Bilot Click Link	css=#emptyCart

