*** Settings ***

Documentation  This is a common file with Bilot's common settings and our test keywords.
#Library	Remote	http://localhost:8270


*** Variables ***

${BROWSER}       firefox
${DELAY}         0.5
${DEFAULT_TIMEOUT}	15 


*** Keywords ***

Run on Failure
	[Documentation]	Executed when an error happens during execution
	Log Variables
	Run Keyword And Ignore Error	Capture Page Screenshot	${SUITE NAME}_${TEST NAME}.png
	Log Source
	

Suite Set Up
	[Documentation]	Setting up the suite settings.
	Log	Starting test
    
Clean Up Test
	[Documentation]	We clean up respurces when the case ends.
	Delete All Cookies
	Run Keyword And Ignore Error	Capture Page Screenshot	
	${previous kw}=	Register Keyword To Run On Failure	Nothing	# Disables run-on-failure functionality and stores the previous kw name in a variable. 	 
	Run Keyword And Ignore Error	Close Browser
	Register Keyword To Run On Failure	${previous kw}	# Restore to the previous keyword.

Clean Up Suite
	[Documentation]	All browsers should be closed when the tests end.
	${previous kw}=	Register Keyword To Run On Failure	Nothing	# Disables run-on-failure functionality and stores the previous kw name in a variable. 
	Run Keyword And Ignore Error	Close All Browsers	# Browser closing may result in errors, even tough the browser is closed. 	
	Register Keyword To Run On Failure	${previous kw}	# Restore to the previous keyword.     
    
Click Element If Not Visible
	[Documentation]	Checks if the given element is present at the page, 
	...				if it isn't, clicks the specified element (e.g., a link to the correct page)
	[Arguments]	${expectedElement}	${elementToClick}
	${status}	${value} =	Run Keyword And Ignore Error	Page Should Contain Element	${expectedElement}
	Run Keyword Unless		'${status}' == 'PASS'	Bilot Click Link	${elementToClick}
	Wait Until Page Contains Element	${expectedElement}

Click Element If Visible
	[Documentation]	
	[Arguments]	${element}
	${status}	${value} =	Run Keyword And Ignore Error	Page Should Contain Element	${element}
	Run Keyword Unless		'${status}' != 'PASS'	Bilot Click Link	${element}

    
Go to URL If Not Visible
	[Documentation]	Checks if the given element is present at the page, 
	...				if it isn't, goes to the given URL (e.g., a link to the correct page)
	[Arguments]	${expectedElement}	${url}
	${status}	${value} =	Run Keyword And Ignore Error	Page Should Contain Element ${expectedElement}
	Run Keyword Unless		'${status}' == 'PASS'	Go To	${url}
	Wait Until Page Contains Element	${expectedElement}
	
Do Not Use Directly Check Input Value
	[Documentation]	check input field value 
	[Arguments]	${selectorForValue}	${expectedValue}
    ${val}=	Get Value	${selectorForValue}
    Should Be True	'${val}' == '${expectedValue}'

Check Input Value
	[Arguments]	${selectorForValue}	${expectedValue}
	Wait Until Keyword Succeeds	10 s	1 s	Do Not Use Directly Check Input Value	${selectorForValue}	${expectedValue}

Check Element Value
	[Arguments]	${selectorForValue}	${expectedValue}
	Wait Until Page Contains Element	${selectorForValue}
	Wait Until Keyword Succeeds	10 s	1 s	Element Text Should Be	${selectorForValue}	${expectedValue}

Bilot Get Element Text
	[Arguments]	${selectorForValue}
	[Return]    ${val}
	Wait Until Page Contains Element	${selectorForValue}
	${val}=     Wait Until Keyword Succeeds	10 s	1 s		Get Text	${selectorForValue}

Bilot Get Element Value
	[Arguments]	${selectorForValue}
	[Return]    ${val}
	Wait Until Page Contains Element	${selectorForValue}
	${val}=     Wait Until Keyword Succeeds	10 s	1 s		Get Value	${selectorForValue}

Set Input Value
	[Documentation]	clears field and sets the value. also waits untill value has been set 
	[Arguments]	${selectorForValue}	${value}
	Wait Until Page Contains Element	${selectorForValue}
	Input Text	${selectorForValue}	${Empty}
	Check Input Value	${selectorForValue}	${Empty}
	Input Text	${selectorForValue}	${value}
	Check Input Value	${selectorForValue}	${value}
	
Bilot Click Link
	[Arguments]	${target}
	Wait Until Page Contains Element	${target}
	Wait Until Keyword Succeeds	15 s	1 s     Click Element	${target}

Bilot Mouse Over
	[Arguments]	${target}
	Wait Until Page Contains Element	${target}
	Sleep   1
	Wait Until Keyword Succeeds	15 s	1 s     Mouse Over	${target}

Bilot Element Attribute Should Be
	[Arguments]	${target}   ${value}
    ${temp}=      Get Element Attribute     ${target}
    Should Be Equal As Strings  ${temp}     ${value}

Bilot Element Text Should Be
	[Arguments]	${target}   ${value}
    ${temp}=    Bilot Get Element Text     ${target}
    Should Be Equal As Strings  ${temp}     ${value}